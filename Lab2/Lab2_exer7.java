package lab02;

import java.util.Scanner;

public class Lab2_exer7{
    public static void main(String args[]){
        int[][] a=new int [10][10];
        int[][] b=new int [10][10];
        int[][] c=new int [10][10];
        Scanner scanner=new Scanner(System.in);
        System.out.println("Enter the size of 2 matrix: ");
        int size=scanner.nextInt();
        System.out.println("Enter matrix A :");
        for(int i=0;i<size;i++)
            for(int j=0;j<size;j++)
                a[i][j]=scanner.nextInt();

        System.out.println("Enter matrix B :");
        for(int i=0;i<size;i++)
            for(int j=0;j<size;j++)
                b[i][j]=scanner.nextInt();


        for(int i = 0;i<size;i++){
            for(int j = 0;j<size;j++){
                c[i][j] = a[i][j]+b[i][j];
                System.out.print(c[i][j]+" ");
            }
            System.out.println();
        }
    }
}
