package hust.soict.ictglobal.date;

public class DateUtils {
    public static int compareTwoDates(MyDate date1, MyDate date2) {
        if (date1.getYear() < date2.getYear()) return -1;
        else if (date1.getYear() > date2.getYear()) return 1;
        else {
            if (date1.getMonth() < date2.getMonth()) return -1;
            else if (date1.getMonth() > date2.getMonth()) return 1;
            else {
                if (date1.getDay() < date2.getDay()) return -1;
                else if (date1.getDay() > date2.getDay()) return 1;
                else return 0;
            }
        }
    }

    public static void sortDates(MyDate[] comparedDate) {
        for (int i = 0; i < comparedDate.length - 1; i++) {
            for (int j = i + 1; j < comparedDate.length; j++) {
                if (compareTwoDates(comparedDate[i], comparedDate[j]) < 1) {
                    MyDate tempDate;
                    tempDate = comparedDate[i];
                    comparedDate[i] = comparedDate[j];
                    comparedDate[j] = tempDate;
                }
            }
        }
    }
}
