package hust.soict.ictglobal.lab02;

import java.util.Arrays;
import java.util.Scanner;

public class Lab2_exer6 {
    public static void main(String[] args) {
        int [] array=new int [10];
        int size,sum=0;
        double average;
        Scanner scanner=new Scanner(System.in);
        System.out.println("Enter the size of the array:");
        size=scanner.nextInt();
        System.out.println("Enter value of the array:");
        for(int i=0;i<size;i++)
            array[i]=scanner.nextInt();
        System.out.println("Sort:");
        for(int i=0;i<size;i++)
            for(int j=0;j<size;j++)
                if(array[i]<array[j]) {
                    int temp;
                    temp=array[i];
                    array[i]=array[j];
                    array[j]=temp;
                }
        for (int i = 0; i <size ; i++) System.out.print("  "+array[i]);
        System.out.println();
        for(int i=0;i<size;i++) sum+=array[i];
        System.out.println("Sum of array: "+sum);
        System.out.println("Average value: "+(double)sum/size);
    }
}
