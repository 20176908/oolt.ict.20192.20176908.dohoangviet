package hust.soict.ictglobal.aims.media;

import java.util.ArrayList;
import java.util.List;

public class Book extends Media {
    private List<String> authors = new ArrayList<String>();

    public List<String> getAuthors() {
        return authors;
    }

    public void setAuthors(List<String> authors) {
        this.authors = authors;
    }

    public Book(String title, String category, List<String> authors) {
        super(title, category);
        this.authors = authors;
    }

    public Book(int id, String title, String category, List<String> authors) {
        super(id, title, category);
        this.authors = authors;
    }

    public Book(String title) {
        super(title);
    }

    public void addAuthor(String authorName) {
        if (!this.getAuthors().contains(authorName)) this.getAuthors().add(authorName);
        else System.out.println("This author name is already in the author list");
    }

    public void removeAuthor(String authorName) {
        if (this.getAuthors().contains(authorName)) this.getAuthors().remove(authorName);
        else System.out.println("This author name is not in the author list");
    }
}
