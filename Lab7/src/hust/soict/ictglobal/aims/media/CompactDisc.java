package hust.soict.ictglobal.aims.media;

import java.util.ArrayList;

public class CompactDisc extends Disc implements Playable {
    private String artist;
    private int length;
    private ArrayList<Track> tracks = new ArrayList<>();

    public String getArtist() {
        return artist;
    }

    public void setArtist(String artist) {
        this.artist = artist;
    }

    public CompactDisc(String title, String category, String artist) {
        super(title, category);
        this.artist = artist;
    }

    public CompactDisc(String title) {
        super(title);
    }

    public void addTrack(Track track) {
        if (!this.tracks.contains(track)) this.tracks.add(track);
        else System.out.println("This track is already in the track list");
    }

    public void removeTrack(Track track) {
        if (this.tracks.contains(track)) this.tracks.remove(track);
        else System.out.println("This track is not in the track list ");
    }

    public int getLength(CompactDisc compactDisc) {
        int length = 0;
        for (Track track : this.tracks) {
            length += track.getLength();
        }
        return length;
    }

    @Override
    public void play() {
        System.out.println("Playing CD :" + this.getTitle());
        for (Track track : this.tracks) {
            System.out.println("Playing track: " + track.getTitle());
            System.out.println("DVD length: " + track.getLength());
        }
    }
}
