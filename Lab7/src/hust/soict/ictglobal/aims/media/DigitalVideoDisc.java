package hust.soict.ictglobal.aims.media;

public class DigitalVideoDisc extends Disc implements Playable {
    public DigitalVideoDisc(String title) {
        super(title);
    }

    public DigitalVideoDisc(String s, String title) {
        super(s, title);
    }

    public DigitalVideoDisc(String title, String category,String director) {
        super(title, category, director);
    }

    @Override
    public void play() {
        System.out.println("Playing DVD: " + this.getTitle());
        System.out.println("DVD length: " + this.getLength());

    }
}
