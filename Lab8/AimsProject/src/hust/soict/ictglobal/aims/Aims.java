package hust.soict.ictglobal.aims;

import hust.soict.ictglobal.aims.media.*;
import hust.soict.ictglobal.aims.order.Order;

import java.util.*;

public class Aims {
    public static void showMenu() {
        System.out.println("Order Management Application: ");
        System.out.println("--------------------------------");
        System.out.println("1. Create new order");
        System.out.println("2. Add item to the order");
        System.out.println("3. Delete item by title");
        System.out.println("4. Display the items list of order");
        System.out.println("5. Play the track list of chosen CD");
        System.out.println("0. Exit");
        System.out.println("--------------------------------");
        System.out.println("Please choose a number: 0-1-2-3-4");
    }

    public static void showMiniMenu() {
        System.out.println("Which item you want to add ? ");
        System.out.println("--------------------------------");
        System.out.println("1.Book");
        System.out.println("2.DVD");
        System.out.println("3.CD");
        System.out.println("0.Exit");
        System.out.println("--------------------------------");
    }

    public static void showMiniMenu1() {
        System.out.println("Do you want to add some more tracks ?");
        System.out.println("1. Yes");
        System.out.println("2. No");
    }

    public static void main(String[] args) {
        Thread thread = new Thread(new MemoryDaemon());
        thread.setDaemon(true);
        thread.start();
        Scanner scanner = new Scanner(System.in);
        int n = 1;
        boolean isOption1Chosen = false;
        Order order = new Order();
        while (n > -1 || n < 5) {
            showMenu();
            n = Integer.parseInt(scanner.nextLine());
            if (n == 1) {
                System.out.println("A new order with is created");
                isOption1Chosen = true;
            }
            if (n == 2) {
                if (!isOption1Chosen) System.out.println("You must create an order by option 1 first !");
                else {
                    int choice = 1;
                    String tempTitle, tempCategory, tempAuthors, tempArtist;
                    List<String> authors = new ArrayList<>();
                    while (choice > -1 || choice < 3) {
                        showMiniMenu();
                        choice = Integer.parseInt(scanner.nextLine());
                        if (choice == 0) break;
                        System.out.println("Title :");
                        tempTitle = scanner.nextLine();
                        System.out.println("Category: ");
                        tempCategory = scanner.nextLine();
                        if (choice == 1) {
                            System.out.println("Input authors, each one is separated by a comma:");
                            tempAuthors = scanner.nextLine();
                            authors = Arrays.asList(tempAuthors.split(","));
                            Book book = new Book(tempTitle, tempCategory, authors);

                            order.getItemsOrdered().add(book);
                        }
                        if (choice == 2) {
                            DigitalVideoDisc digitalVideoDisc = new DigitalVideoDisc(tempTitle, tempCategory);
                            order.getItemsOrdered().add(digitalVideoDisc);
                        }
                        if (choice == 3) {
                            String tempTrackTitle;
                            int tempTrackLength, lastChoice = 1;
                            Track tempTrack;
                            System.out.println("Artist: ");
                            tempArtist = scanner.nextLine();
                            CompactDisc compactDisc = new CompactDisc(tempTitle, tempCategory, tempArtist);
                            System.out.println("Input list of tracks below");

                            while (lastChoice == 1) {
                                System.out.println("Title: ");
                                tempTrackTitle = scanner.nextLine();
                                System.out.println("Length: ");
                                tempTrackLength = Integer.parseInt(scanner.nextLine());
                                tempTrack = new Track(tempTrackTitle, tempTrackLength);
                                compactDisc.addTrack(tempTrack);
                                showMiniMenu1();
                                lastChoice = Integer.parseInt(scanner.nextLine());
                            }
                            order.getItemsOrdered().add(compactDisc);
                        }
                    }

                }

            }
            if (n == 3) {
                if (!isOption1Chosen) System.out.println("You must create an order by option 1 first !");
                else {
                    int index;
                    String tempTitle;
                    if (order.getItemsOrdered().size() != 0) {
                        System.out.println("Input title of item you want to delete :");
                        tempTitle = scanner.nextLine();
                        for (index = 0; index < order.getItemsOrdered().size(); index++)
                            if (order.getItemsOrdered().get(index).getTitle().equals(tempTitle)) break;
                        order.getItemsOrdered().remove(index);
                        System.out.println("Item is deleted !");
                    } else System.out.println("The item list is empty");
                }
            }
            if (n == 4) {
                if (!isOption1Chosen) System.out.println("You must create an order by option 1 first !");
                else {
                    for (Media media : order.getItemsOrdered()) {
                        System.out.println("Title: " + media.getTitle());
                        System.out.println("Category: " + media.getCategory());
                        if (media instanceof Book) {
                            System.out.print("Authors: ");
                            for (String auth : ((Book) media).getAuthors()) System.out.println(auth);
                        }
                        if (media instanceof CompactDisc) {
                            System.out.println("Artist: " + ((CompactDisc) media).getArtist());
                            System.out.print("Length of all tracks : ");
                            System.out.println(((CompactDisc) media).getLength() + " minutes");
                        }
                    }
                }
            }
            if (n == 5) {
                if (!isOption1Chosen) System.out.println("You must create an order by option 1 first !");
                else {
                    System.out.println("Choose the title of the CD/DVD you want to play");
                    for (Media media : order.getItemsOrdered()) {
                        if (media instanceof DigitalVideoDisc) ((DigitalVideoDisc) media).play();
                        if (media instanceof CompactDisc) ((CompactDisc) media).play();
                    }
                }
            }
            if (n == 0) break;
        }
        List<DigitalVideoDisc> discs = new ArrayList<DigitalVideoDisc>();
        //Collection collection = new ArrayList();
        DigitalVideoDisc dvd1 = new DigitalVideoDisc("Star Wars", "Sci-fi",5.5f);
        DigitalVideoDisc dvd2 = new DigitalVideoDisc("The Lion King", "Animation",7.2f);
        DigitalVideoDisc dvd3 = new DigitalVideoDisc("Aladdin", "Animation",2.0f);
        DigitalVideoDisc dvd4 = new DigitalVideoDisc("Shazam", "DCEU",1.0f);
        discs.add(dvd1);
        discs.add(dvd2);
        discs.add(dvd3);
        discs.add(dvd4);
        Iterator iterator = discs.iterator();
        System.out.println("--------------------------------");
        System.out.println("The DVDs currently in the order are: ");
        while (iterator.hasNext()) {
            System.out.println(((DigitalVideoDisc) iterator.next()).getTitle());
        }
        // sort DVDs based on compareTo() method
        Collections.sort(discs);
        iterator = discs.iterator();

        System.out.println("-------------------------------");
        System.out.println("The DVDs in sorted order are: ");
        while(iterator.hasNext()){
            System.out.println
                    (((DigitalVideoDisc)iterator.next()).getTitle());
        }
        System.out.println("--------------------------------");
    }
}