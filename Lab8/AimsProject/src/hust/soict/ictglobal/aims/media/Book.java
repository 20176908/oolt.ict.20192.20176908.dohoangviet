package hust.soict.ictglobal.aims.media;

import java.util.*;

public class Book extends Media implements Comparable {
    private List<String> authors = new ArrayList<String>();
    private String content;
    private List<String> contentTokens = new ArrayList<>();
    private Map<String, Integer> wordFrequency = new TreeMap<>();

    public List<String> getAuthors() {
        return authors;
    }

    public void setAuthors(List<String> authors) {
        this.authors = authors;
    }


    public Book(String title, String category, List<String> authors) {
        super(title, category);
        this.authors = authors;
    }

    public Book(String title) {
        super(title);
    }

    public void addAuthor(String authorName) {
        if (!this.getAuthors().contains(authorName)) this.getAuthors().add(authorName);
        else System.out.println("This author name is already in the author list");
    }

    public void removeAuthor(String authorName) {
        if (this.getAuthors().contains(authorName)) this.getAuthors().remove(authorName);
        else System.out.println("This author name is not in the author list");
    }

    @Override
    public int compareTo(Object o) {
        Book book = (Book) o;
        return this.getTitle().compareTo(book.getTitle());
    }

    public String getContent() {
        return content;
    }

    public List<String> getContentTokens() {
        return contentTokens;
    }

    public Map<String, Integer> getWordFrequency() {
        return wordFrequency;
    }

    public void processContent() {
        int i;
        String[] temp = this.getContent().split(" ");
        for (i = 0; i < temp.length; i++) {
            this.getContentTokens().add(temp[i]);
        }
        Collections.sort(this.getContentTokens());
        i = 0;
        int count = 0, j = 0;
        while (i<this.getContentTokens().size()){
            while(this.getContentTokens().get(i).equals(this.getContentTokens().get(j))) {
                count++;
                j++;
                if(j>=this.getContentTokens().size()) break;
            }
            this.getWordFrequency().put(this.getContentTokens().get(i),count);
            i=j;
            count=0;
        }
        //Collections.sort(this.getWordFrequency());
    }

    public Book(String title, String content) {
        super(title);
        this.content = content;
    }

    @Override
    public String toString() {
        return "Book{" +
                ", content='" + content + '\'' +
                ", contentLength=" + contentTokens.size() +
                ", contentTokens=" + contentTokens +
                ", wordFrequency=" + wordFrequency +
                '}';
    }
}
